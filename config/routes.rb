Rails.application.routes.draw do
  root to: 'articles#index'
  resources :articles

  resources :authors, excpet: [:new, :destroy]
  get '/register', to: 'authors#new', as: 'register'
  get '/login', to: 'sessions#new', as: 'login'
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy', as: 'logout'
end
