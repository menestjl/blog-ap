class SessionsController < ApplicationController
  include SessionsHelper
  def new
  end

  def create
    author = Author.find_by(email: params[:session][:email])
    if author and author.authenticate(params[:session][:password])
      flash[:success] = "Your logged in!"
      #Log in User
      log_in(author)
      redirect_to articles_path
    else
      flash.now[:danger] = "Password and email do not match"
      render :new
    end
  end

  def destroy
    session[:author_id] = nil
    @current_user = nil
    flash[:success] = "You are logged out"
    redirect_to root_path
  end

end
