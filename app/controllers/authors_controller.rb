class AuthorsController < ApplicationController

  def new
    @author = Author.new
  end

  def create
    @author = Author.new(author_params)
    if @author.save
      flash[:success] = "Welcome to Blog App!"
      log_in(@author)
      redirect_to articles_path
    else
      flash.now[:danger] = "Signup Failed"
      render :new
    end

  end

  private
    def author_params
      params.require(:author).permit(:name, :email, :password, :password_confirmation)
    end

end
