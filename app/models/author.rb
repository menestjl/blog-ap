class Author < ActiveRecord::Base
  before_save :email_downcase
  validates :name, presence: true
  validates :email, presence: true, uniqueness: {case_sensitive: false}

  has_secure_password

  private
    def email_downcase
      self.email = email.downcase
    end
end
