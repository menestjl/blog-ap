require 'rails_helper'

RSpec.describe Author, type: :model do

  before do 
    @user = Author.new(name: "Justin", email: "foobar@example.com", password: "password", password_confirmation: "password")
    @user2 = Author.new(name: "Emily", email: "foobar@example.com", password: "password", password_confirmation: "password")

  end

  scenario "fail to save blank name" do 
    @user.name = ""
    expect(@user.save).to eq(false)
  end

  scenario "fail to save blank email" do 
    @user.email = ""
    expect(@user.save).to eq(false)
  end

  scenario "user with same email will not save" do
    expect(@user.save).to eq(true)
    expect(@user2.save).to eq(false)
  end

end
