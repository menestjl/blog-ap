require 'rails_helper'

RSpec.feature "Register new User" do 
  scenario "A user successfully signs up" do 

    visit "/"

    click_link("Register")
    fill_in 'Name', with: "User"
    fill_in "Email", with: "example@example.com"
    fill_in "Password", with: "password"
    fill_in "Confirm Password", with: "password"

    before_count = Author.all.count
    click_button("Register")

    expect(page).to have_content("Welcome to Blog App!")
    expect(page.current_path).to eq(articles_path)
    expect(Author.all.count).to eq(before_count+1)

  end

  scenario "A user fails to sign up" do 

    visit "/"
    click_link("Register")
    fill_in "Name", with: "User"
    fill_in "Email", with: ""
    fill_in "Password", with: ""
    fill_in "Confirm Password", with: ""

    before_count = Author.all.count
    click_button("Register")

    expect(page).to have_content("Signup Failed")
    expect(Author.all.count).to eq(before_count)
    

  end
end